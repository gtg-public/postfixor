#!/bin/bash

postconf -e "relayhost = [${SMTP_HOST}]:587" \
"smtp_sasl_auth_enable = yes" \
"smtp_sasl_security_options = noanonymous" \
"smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd" \
"smtp_use_tls = yes" \
"smtp_tls_security_level = encrypt" \
"smtp_tls_note_starttls_offer = yes" \
"smtpd_sasl_auth_enable = yes" \
"cyrus_sasl_config_path = /etc/postfix/sasl" \
"smtpd_sasl_local_domain = $myhostname" \
"smtpd_sasl_path = smtpd" \
"smtpd_sasl_security_options = noanonymous" \
"smtpd_recipient_restrictions = permit_sasl_authenticated, permit_mynetworks, reject_unauth_destination" \
"broken_sasl_auth_clients = yes"


echo "[${SMTP_HOST}]:587 ${SMTP_USERNAME}:${SMTP_PASSWORD}" > /etc/postfix/sasl_passwd

postmap hash:/etc/postfix/sasl_passwd

## Allow for overriding every config
for e in ${!POSTFIX_*} ; do postconf -e "${e:8}=${!e}" ; done

rm -f /var/spool/postfix/pid/master.pid

cp /etc/resolv.conf  /var/spool/postfix/etc/resolv.conf

service saslauthd restart

exec "$@"

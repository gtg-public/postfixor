FROM debian:buster-slim

# This container uses the new "postfix start-fg" command
# that was developed specifically for container use
# @see http://www.postfix.org/announcements/postfix-3.3.0.html

# Preselections for installation
RUN echo postfixor > /etc/hostname \
  && echo "postfix postfix/mailname string postfixor.gebbersfarms.com" | debconf-set-selections \
  && echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections \
  && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
    sasl2-bin \
    postfix \
    libsasl2-modules \
    ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


RUN postconf -e 'smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt' && \
    postconf -e 'syslog_name=postfixor' && \
    postconf -e 'smtpd_use_tls=no' && \
    postconf -e 'maillog_file=/dev/stdout' && \
    postconf -e 'mynetworks=0.0.0.0/0' # override mynetworks to restrict ips

COPY docker/smtpd.conf /etc/postfix/sasl/smtpd.conf
COPY docker/entrypoint.sh /
RUN chmod +x /entrypoint.sh


RUN addgroup smtp && echo smtp > /etc/postfix/sasl-group.allowed
COPY docker/saslauthd-postfix /etc/default/saslauthd
RUN mkdir -p  /var/spool/postfix/var/run/saslauthd && usermod -aG sasl postfix

RUN useradd -rm \
    -d /home/postfixor \
    -s /bin/bash \
    -g root \
    -G sudo,smtp \
    -p "$(openssl passwd -1 secret)" \
    -u 1001 postfixor

# dWJ1bnR1 c2VjcmV0

EXPOSE 25

ENTRYPOINT ["/entrypoint.sh"]
CMD ["postfix", "start-fg"]
